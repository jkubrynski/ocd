package com.bottega.payments;

import io.restassured.module.mockmvc.RestAssuredMockMvc;
import org.junit.Before;
import org.mockito.Mockito;

public abstract class AbstractContractTest {

    @Before
    public void setUp() {
        PaymentService mock = Mockito.mock(PaymentService.class);

        Mockito.when(mock.getPayment("paid"))
                .thenReturn(new Payment("paid", Payment.PaymentStatus.PAID));

        RestAssuredMockMvc.standaloneSetup(new PaymentController(mock));
    }
}
