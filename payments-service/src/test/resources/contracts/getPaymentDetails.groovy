package contracts

import org.springframework.cloud.contract.spec.Contract

Contract.make {
    request {
        method('GET')
        url('/payments/paid')

        headers {
            accept(applicationJson())
        }
    }

    response {
        status(200)
        headers {
            contentType(applicationJson())
        }

        body(
                'paymentId': 'paid',
                'status': 'PAID'
        )

    }
}