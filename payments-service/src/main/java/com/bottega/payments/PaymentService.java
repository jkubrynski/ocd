package com.bottega.payments;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.lang.invoke.MethodHandles;

import static com.bottega.payments.Payment.PaymentStatus.PAID;

@Service
public class PaymentService {

    private final static Logger LOG = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    public Payment getPayment(String paymentId) {
        LOG.info("Selecting payment [paymentId={}]", paymentId);
        return new Payment(paymentId, PAID);
    }
}
