package com.bottega.payments;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.lang.invoke.MethodHandles;

import static com.bottega.payments.Payment.PaymentStatus.PAID;

@RestController
@RequestMapping("/payments")
public class PaymentController {

    private final static Logger LOG = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    private final PaymentService paymentService;

    public PaymentController(PaymentService paymentService) {
        this.paymentService = paymentService;
    }

    @GetMapping(value = "/{paymentId}", produces = MediaType.APPLICATION_JSON_VALUE)
    Payment getPayment(@PathVariable String paymentId) {
        LOG.info("Retrieving payment status [paymentId={}]", paymentId);
        return paymentService.getPayment(paymentId);
    }
}
