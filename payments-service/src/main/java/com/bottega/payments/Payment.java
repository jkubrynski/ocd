package com.bottega.payments;

public class Payment {
    private final String paymentId;
    private final PaymentStatus paymentStatus;

    public Payment(String paymentId, PaymentStatus paymentStatus) {
        this.paymentId = paymentId;
        this.paymentStatus = paymentStatus;
    }

    public String getPaymentId() {
        return paymentId;
    }

    public PaymentStatus getPaymentStatus() {
        return paymentStatus;
    }

    public PaymentStatus getStatus() {
        return paymentStatus;
    }

    enum PaymentStatus {
        NEW, PAID, PENDING
    }
}
