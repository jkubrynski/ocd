1. Start consul - ./consul/run.sh
2. Hystrix - http://localhost:8081/hystrix/monitor?stream=http%3A%2F%2Flocalhost%3A8081%2Factuator%2Fhystrix.stream




Links:
https://docs.microsoft.com/bs-latn-ba/azure/architecture/patterns/bulkhead
https://resilience4j.readme.io/docs/circuitbreaker
https://github.com/openzipkin/b3-propagation
https://micronaut.io


Books:
https://www.amazon.com/Working-Effectively-Legacy-Michael-Feathers/dp/0131177052

https://www.amazon.com/Enterprise-Patterns-MDA-Building-Archetype/dp/032111230X/ref=sr_1_1?keywords=mda&qid=1576577172&s=books&sr=1-1

http://www.brendangregg.com/sysperfbook.html

http://www.brendangregg.com/bpf-performance-tools-book.html

Push:
+ w projektowym repo - developerzy maja wpływ na zmiany i jest spojnie
+ proste
+ wersja properties spięta z wersja aplikacji

- Trzeba robić deploy przy zmianie konfiguracji
- Config per aplikacja - potrzebuje i tak globalnego mechanizmu szyfrowania

Pull:
+ Mozna zmienić konfiguracje bez redeploy aplikacji
+ czesto ma wbudowany mechanizm szyfrowania

- Nie ma kontroli nad momentem zaciągania konfiguracji przez aplikacje - patrz restart poda
- Configuration server staje sie SPOF
- Komplikuje infre

