package com.bottega.cart;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(value = "payments-service", fallback = PaymentsServiceClientFallback.class)
public interface PaymentsServiceClient {

    @GetMapping(value = "/payments/{paymentId}", produces = MediaType.APPLICATION_JSON_VALUE)
    Payment getPaymentDetails(@PathVariable("paymentId") String cartId);

}
