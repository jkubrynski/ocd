package com.bottega.cart;

public class Payment {
    private final String paymentId;
    private final String status;

    public Payment(String paymentId, String status) {
        this.paymentId = paymentId;
        this.status = status;
    }

    public String getPaymentId() {
        return paymentId;
    }

    public String getStatus() {
        return status;
    }

}
