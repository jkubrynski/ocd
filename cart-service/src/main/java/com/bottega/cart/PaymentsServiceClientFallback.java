package com.bottega.cart;

import org.springframework.stereotype.Service;

@Service
public class PaymentsServiceClientFallback implements PaymentsServiceClient {
    @Override
    public Payment getPaymentDetails(String cartId) {
        return new Payment(cartId, "UNKNOWN");
    }
}
