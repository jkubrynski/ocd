package com.bottega.cart;

public class Cart {
    private final String cartId;
    private final String paymentStatus;

    Cart(String cartId, String paymentStatus) {
        this.cartId = cartId;
        this.paymentStatus = paymentStatus;
    }

    public String getCartId() {
        return cartId;
    }

    public String getPaymentStatus() {
        return paymentStatus;
    }
}
