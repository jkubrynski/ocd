package com.bottega.cart;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.lang.invoke.MethodHandles;

@Service
class CartService {

    private final static Logger LOG = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    private final PaymentsServiceClient paymentsServiceClient;

    CartService(PaymentsServiceClient paymentsServiceClient) {
        this.paymentsServiceClient = paymentsServiceClient;
    }

    Cart getCart(String cartId) {
        LOG.info("Invoking payment-service [cartId={}]", cartId);
        Payment payment = paymentsServiceClient.getPaymentDetails(cartId);
        if (!"PAID".equals(payment.getStatus())) {
            throw new IllegalStateException();
        }
        return new Cart(cartId, payment.getStatus());
    }
}
