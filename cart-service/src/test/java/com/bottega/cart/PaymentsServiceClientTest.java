package com.bottega.cart;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.contract.stubrunner.spring.AutoConfigureStubRunner;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
@AutoConfigureStubRunner(ids = "com.bottega:payments-service:+:stubs")
class PaymentsServiceClientTest {

	@Autowired
	PaymentsServiceClient paymentsServiceClient;

	@Test
	void shouldGetPaymentDetails() {
		// when
		Payment paymentDetails = paymentsServiceClient.getPaymentDetails("paid");

		// then
		assertThat(paymentDetails.getStatus()).isEqualToIgnoringCase("PAID");
	}

}
